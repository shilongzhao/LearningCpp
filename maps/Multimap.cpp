//
// Created by ZhaoShilong on 30/06/2018.
//
#include <iostream>
#include <map>
using namespace std;

int main() {
    // multimap cannt use index subscriptor to insert
    // multimap allows duplicate keys...

    multimap<int, string> lookup;
    lookup.insert(make_pair(30, "mike"));
    lookup.insert(make_pair(10, "vicky"));
    lookup.insert(make_pair(30, "raj"));
    lookup.insert(make_pair(20, "vencent"));


    for (auto &look : lookup) {
        cout << look.first << " - " << look.second << endl;
    }

    cout << "finding range in map " << endl;
//    pair<multimap<int,string>::iterator,multimap<int,string>::iterator>
    auto range = lookup.equal_range(30);

    for (auto it = range.first; it != range.second; it++) {
        cout << it->first << " - " << it->second << endl;
    }

}