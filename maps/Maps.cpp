//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <map>
using namespace std;

int main() {
    map<string, int>  ages;

    ages["vic"] = 33; // keys must be unique
    ages["zhao"] = 29;
    ages["raj"] = 30;
    ages["mike"] = 40;

    // insert 1
    pair<string, int> peter("peter", 80);
    ages.insert(peter);

    //insert 2
    ages.insert(pair<string, int>("sim", 90));

    //insert 3
    ages.insert(make_pair("dan", 56));

    cout << ages["zhao"] << endl;

    if (ages.find("mark") == ages.end()) { // find() returns an iterator to the element,
        // if an element with specified key is found, or map::end otherwise.
        cout << "mark not found in map" << endl;
    }

    map<string, int>::iterator itx = ages.find("vic");
    if (itx != ages.end()) {
        cout << "vic is found! " << itx->second <<  endl;
    }

//    for (map<string, int>::iterator it = ages.begin(); it != ages.end(); it++) {
//        cout << it->first << " --> " << it->second << endl;
//    } // ordered key output


    for (auto &age : ages) { // pair<string, int> type
        cout << age.first << " --> " << age.second << endl;
    } // ordered key output


    return 0;
}