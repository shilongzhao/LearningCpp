//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <map>
using namespace std;

class Person {
private:
    string name;
    int age;
public:
    Person() { // no parameter constructor needed
        cout << "no parameter constructor running" << endl;
    }

    Person(const Person &other) { // a copy constructor
        cout << "copy constructor running" << endl;
        name = other.name;
        age  = other.age;
    }

    Person(string name, int age): name(name), age(age) {
        cout << "parameterized constructor running" << endl;
    }

    void print() {
        cout << name << ": " << age << endl;
    }
};

int main() {
    map<int, Person> people;
    people[0] = Person("mike", 40);
    people[4] = Person("alex", 60);
    people[2] = Person("vik", 30);

    people.insert(make_pair(5, Person("nadal", 32)));

    for (map<int, Person>::iterator it = people.begin(); it != people.end(); it++) {
        (it->second).print();
    } // ordered output by keys
}