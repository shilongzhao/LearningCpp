//
// Created by Shilong Zhao on 03/07/2018.
//

#include <unordered_set>
#include <string>
#include <iostream>

using namespace std;

class HashableStudent {
private:
    int id;
    string name;
public:
    HashableStudent() :id (0), name("") {

    }

    HashableStudent(int id, string name): id(id), name(name) {

    }

    int getId() const {
        return id;
    }

    string getName() const {
        return name;
    }


};
size_t hasher(const HashableStudent &student)  {
    return hash<int>() (student.getId()) + hash<string> () (student.getName());
}

bool eq(const HashableStudent &s1, const HashableStudent &s2) {
    return s1.getId() == s2.getId();
}

int main() {
    using student_set = unordered_set<HashableStudent, decltype(hasher) *, decltype(eq) *>;
    student_set  students(10, hasher, eq);

    students.insert({1, "zhao"});
    students.insert({2, "zhao"});

    for (auto &s: students) {
        cout << s.getId() << " - " << s.getName() << endl;
    }

}