//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <map>
using namespace std;

class Person {
public:
    Person() {
        cout << "constructing default people " << endl;
    }
    Person(string const &name, int age) : name(name), age(age) {
        cout << "constructing people " << name << " age " << age << endl;
    }
    Person(const Person &other): name(other.name), age(other.age) {
        cout << "copying people" << endl;
    }
    void print() const { // const means no class fields will be changed in this function
        cout << name << " - " << age << endl;
    }
    // needs a compare function or < operator when used as a key in the map
    bool operator < (Person const &other) const {
        if (name  == other.name) {
            return age < other.age;
        }
        return name < other.name; // map will order keys by comparing name
    }
private:
    string name;
    int age;
};

int main() {
    map<Person, int> people;
    people[Person("vix", 40)] = 180;

    people.insert(make_pair(Person("alex", 41), 185));
    people.insert(make_pair(Person("laura", 38), 170));
    people.insert(make_pair(Person("laura", 33), 188)); // Note: duplicate key?


    for (map<Person, int>::iterator it = people.begin(); it != people.end(); it++) {
        cout << it->second << " " << flush;
        (it->first).print();
    }
}

