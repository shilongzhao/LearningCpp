//
// Created by Shilong Zhao on 13/08/2018.
//

/**
 * https://leetcode.com/problems/jewels-and-stones/description/
 *
 */
#include <string>
#include <map>
using namespace std;

class Solution {
public:
    int numJewelsInStones(string J, string S) {
        map<char, int> count;
        for (char c : S) {
            if (count.find(c) == count.end()) {
                count[c] = 1;
            }
            else {
                count[c] += 1;
            }
        }
        int res = 0;
        for (char c: J) {
            if (count.find(c) != count.end()) {
                res += count[c];
            }
        }
        return res;
    }
};