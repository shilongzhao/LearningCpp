/**
 * 91. Decode Ways
 * https://leetcode.com/problems/decode-ways/description/
 *
 * Created by Shilong Zhao on 29/08/2018.
 */
#include <vector>
#include <string>
using namespace std;

class Solution {

public:
    int numDecodings(string s) {
        vector<vector<int>> m(s.size(), vector<int>(s.size()));
        for (int i = 0; i < s.size(); i++) {
            m[i][i] = (s[i] != '0') ? 1 : 0;
        }
//        for (int i = 0; i < s.size() - 1; i++) {
//            if (s[i] == '0') {
//                m[i][i+1] = 0;
//                continue;
//            }
//
//            int k = (s[i] - '0') * 10 + (s[i+1] - '0');
//
//            if (k == 10 || k == 20)     m[i][i+1] = 1;
//            else if (k > 0 && k <= 26)  m[i][i+1] = 2;
//            else                        m[i][i+1] = 0;
//        }

        for (int l = 2; l <= s.size(); l++) {
            for (int i = 0; i + l <= s.size(); i++) {
                int end = i + l - 1;
                if (s[i] == '0') {
                    m[i][end] = 0;
                    continue;
                }

                int k = (s[end - 1] - '0') * 10 + (s[end] - '0');
                if (k == 10 || k == 20) {

                }
            }
        }

        return m[0][s.size() - 1];
    }
};