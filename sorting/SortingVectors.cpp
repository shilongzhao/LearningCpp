//
// Created by ZhaoShilong on 01/07/2018.
//

#include <iostream>
#include <vector>
using namespace std;

class Student {
private:
    int id;
    string name;
public:
    Student() = default;
    Student(int id, string name): id(id), name(name) {

    }
    void print() const { // remember the const
        cout << "student[" << id << ", " << name << "]" << endl;
    }
    bool operator < (Student const &other) const { //
        return name < other.name;
    }
    friend bool compareId(const Student &a, const Student &b); // friend is allowed to access private members
};

bool compareId(const Student &a, const Student &b) {
    return a.id < b.id;
}

int main() {
    vector<Student> students;
    students.push_back(Student(1, "mike"));
    students.push_back(Student(5, "vic"));
    students.push_back(Student(4, "dan"));
    students.push_back(Student(2, "franz"));
    students.push_back(Student(3, "robert"));


//    sort(students.begin(), students.end());
    sort(students.begin(), students.end(), compareId); // function pointer as parameter
    for (const Student &s: students) {
        s.print();
    }
}
