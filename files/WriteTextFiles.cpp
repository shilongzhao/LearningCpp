//
// Created by ZhaoShilong on 28/06/2018.
//

#include <iostream>
#include <fstream> // functions for file manipulation

using namespace std;

int main() {
    ofstream outFile; // output file stream

    string filename = "text.txt";
    outFile.open(filename);
    if (outFile.is_open()) {
        outFile << "Hello" << endl;
        outFile << "This is second line" << endl;
        outFile.close(); // always close the stream
    }
    else {
        cout << "Error: " << filename << endl;
    }
}