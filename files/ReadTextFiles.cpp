//
// Created by ZhaoShilong on 28/06/2018.
//
#include <iostream>
#include <fstream>

using namespace std;

int main() {
    string filename = "text.txt";

    ifstream in;
    in.open(filename);

    if (in.is_open()) {
        string line;

        while (!in.eof()) { // while(in) {
            getline(in, line);
            cout << line << endl;
        }

        in.close();
    }
    else {
        cout << "cannot open file" << endl;
    }
}
