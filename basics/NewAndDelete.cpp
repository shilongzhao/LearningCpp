//
// Created by Shilong Zhao on 03/07/2018.
//
#include <iostream>
using namespace std;

class ArrayTest{
public:
    unsigned int size;
    explicit ArrayTest(unsigned int size){
        this->size = size;
        cout << "constructing test " << this->size << endl;
    }

    ~ArrayTest() {
        cout << "destroying test " << this->size << endl;
    }

};

int main() {

    ArrayTest test(10);

    auto *ptr = new ArrayTest(15);
    delete ptr; // new calls constructor, delete calls destructor

}