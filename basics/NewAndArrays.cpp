//
// Created by Shilong Zhao on 03/07/2018.
//
/*
 *
 */
#include <iostream>
using namespace std;
class Element {
public:
    Element():id(0){
        cout << "constructing element " << this->id << endl;
    };

    explicit Element(int id) :id(id) {
        cout << "constructing element " << this->id << endl;
    }

    ~Element() {
        cout << "destructing element " << this->id << endl;
    }
private:
    int id;
};

int main() {
    // elements is an array of Element, every element is initialized;
    Element *elements = new Element[5];
    delete [] elements;
    // elementPointers is an array of pointers, the poninters are not initialized.
    Element **elementPointers = new Element *[5];
    delete [] elementPointers;
}
