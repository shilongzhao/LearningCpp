//
// Created by ZhaoShilong on 30/06/2018.
//
#include <iostream>
using namespace std;

class Person {
public:
    string name;
    int age;
    Person() = default;

    Person(string name, int age): name(name), age(age) {

    }
};
int main() {
    int const bufSize = 10;
//    bufSize = 100; // error cannot assign to const

    int height = 180;
    int const &ref = height;
    height = 185;
//    ref = 190; // KO, ref is const
    cout << ref << endl; // OK, 185

    Person p1("vic", 32);
    const Person &rp1 = p1;
    p1.age = 23; // OK, age = 23
    cout << p1.name << " " << p1.age << endl;

    Person const *pp1 = &p1;
    p1.age = 13; // OK, age = 13
//    (*pp1).age = 22; // KO, *pp1 is a constant Person (read the pp1 decalaration from right to left)
    cout << pp1->name << " " << pp1->age << endl;

    Person const p2("alex", 76);
    pp1 = &p2; // OK, now points to p2
//    p2.age = 0; // KO, p2 is const
//    *pp1 = p2; // KO

    cout << pp1->name << " " << pp1->age << endl;

    Person p3("bob", 19);
    Person * const cpp3 = &p3; // NOTE the difference between pp1 and cpp3
//    Person * const cpp2 = &p2; // KO not compatible type;
//    cpp3 = &p1; // KO, cpp3 is read only
    cpp3->age = 12; // OK, cpp3 is a constant pointer to Person, cpp3 cannot points to another person.
    cout << cpp3->name << " " << cpp3->age << endl;
//    cpp1 = &p3; // KO, cpp1 is not assignable

}
