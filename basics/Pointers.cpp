

/**
 *
 */
#include <iostream>
using namespace std;

int main() {
    int a = 10;
    int &ra = a;
    int *pa = &a;
    cout << "address of var ref a = " << &ra << endl;
    cout << "address of var a = " << &a << endl; // the same with &ra
    cout << "address of var pa = " << &pa << endl;
    cout << "value of var pa = " << pa << endl;
}