//
// Created by ZhaoShilong on 30/06/2018.
//
#include <iostream>
#include <stack>
#include <queue>
using namespace std;

class Test {
private:
    string name;
public:
    Test() = default;
    Test(string name): name(name) {

    }
    ~Test() {
        cout << name << " destroyed" << endl;
    }
    void print() {
        cout << name << endl;
    }
};

int main(){
    stack<Test> testStack; // LIFO

    testStack.push(Test("vic")); // original object is destroyed, a copy is added to stack, shallow copy
    testStack.push(Test("mike"));
    testStack.push(Test("veronica"));

//    Test &t1 = testStack.top();
//    testStack.pop();
//    t1.print();
//    once popped, the object is destroyed, so be careful with the reference return type

    while (testStack.size() > 0) {
        Test &t2 = testStack.top();
        t2.print();
        testStack.pop();
    }

}