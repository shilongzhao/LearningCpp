//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 * 20. Valid Parentheses
 * https://leetcode.com/problems/valid-parentheses/description/
 */

#include <iostream>
#include <stack>
using namespace std;

class ValidParenthesesSolution {
public:
    bool isValid(string s) {
        stack<char> st;

        for (int i = 0; i < s.size(); i++) {
            if (s[i] == '(' || s[i] == '[' || s[i] == '{') {
                st.push(s[i]);
            }
            else {
                if (st.empty()) {
                    return false;
                }
                switch (s[i]) {
                    case ')':
                        if (st.top() != '(') {
                            return false;
                        }
                        else {
                            st.pop();
                        }
                        break;
                    case ']':
                        if (st.top() != '[') {
                            return false;
                        }
                        else {
                            st.pop();
                        }
                        break;
                    case '}':
                        if (st.top() != '{') {
                            return false;
                        }
                        else {
                            st.pop();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return st.empty();
    }
};
int main() {

}