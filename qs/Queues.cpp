//
// Created by ZhaoShilong on 01/07/2018.
//

//
// Created by ZhaoShilong on 30/06/2018.
//
#include <iostream>
#include <stack>
#include <queue>
using namespace std;

class Test {
private:
    string name;
public:
    Test() = default;
    Test(string name): name(name) {

    }
    ~Test() {
        cout << name << " destroyed" << endl;
    }
    void print() {
        cout << name << endl;
    }
};

int main(){
    queue<Test> testqueue; // FIFO

    testqueue.push(Test("vic")); // original object is destroyed, a copy is added to stack, shallow copy
    testqueue.push(Test("mike"));
    testqueue.push(Test("veronica"));


    while (testqueue.size() > 0) {
        Test &t2 = testqueue.front();
        t2.print();
        testqueue.pop();
    }

}