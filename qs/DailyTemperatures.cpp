//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 * 739. Daily Temperatures
 * https://leetcode.com/problems/daily-temperatures/description/
 *
 * Elements in the stack is guaranteed to be in descending order
 */

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        stack<int> t;
        vector<int> result(temperatures.size());
        t.push(0);
        for (int i = 1; i < temperatures.size(); i++) {
            while ( (!t.empty()) && (temperatures[i] > temperatures[t.top()]) ) {
                int m = t.top();
                result[m] = i - m;
                t.pop();
            }
            t.push(i);
        }

//        while (!t.empty()) {
//            result[t.top()] = 0;
//            t.pop();
//        }
        return result;
    }
};

int main() {
    vector<int> input {73, 74, 75, 71, 69, 72, 76, 73};
    Solution s;

    vector<int> result = s.dailyTemperatures(input);
    for (int &r: result) {
        cout << r << " ";
    }
    cout << endl;
}