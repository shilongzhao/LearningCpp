//
// Created by Shilong Zhao on 14/08/2018.
//

/**
 * https://leetcode.com/problems/projection-area-of-3d-shapes/description/
 */
#include <vector>
#include <numeric>
using namespace std;
class Solution {
public:
    int projectionArea(vector<vector<int>>& grid) {
        vector<int> row_max(grid.size());
        vector<int> col_max(grid[0].size());
        int non_zero = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] > row_max[i]) row_max[i] = grid[i][j];
                if (grid[i][j] > col_max[j]) col_max[j] = grid[i][j];
                if (grid[i][j] != 0) {
                    non_zero++;
                }
            }
        }

        return non_zero +
                accumulate(row_max.begin(), row_max.end(), 0) +
                accumulate(col_max.begin(), col_max.end(), 0);
    }
};