//
// Created by Shilong Zhao on 19/07/2018.
//
/**
 * https://leetcode.com/problems/prime-palindrome/description/
 */
#include <cmath>
#include <iostream>
using namespace std;

class Solution {
    int base;
    int targetLength;
public:
    int primePalindrome(int N) {
        int M = N;
        targetLength = 0;
        while (M != 0) {
            M = M / 10;
            targetLength += 1;
        }
        base = N / pow(10, targetLength / 2);
        int palindrome = 0;
        do {
            cout<< "testing palindrome " << palindrome << endl;
            palindrome = getNextPalindrome();
        }
        while (palindrome < N || !isPrime(palindrome));
        return palindrome;
    }

    int getNextPalindrome() {
        if (base + 1 == pow(10, targetLength - targetLength / 2)) {
            base = base + 1;
            if (targetLength % 2 != 0) {
                base /= 10;
            }
            targetLength += 1;
            return getNextPalindrome();
        }
        else {
            int reverse = base;
            if (targetLength % 2 != 0) {
                reverse /= 10;
            }
            int result = base;
            while (reverse != 0) {
                int d = reverse % 10;
                reverse /= 10;
                result = result * 10 + d;
            }
            base = base + 1;
            return result;
        }
    }
    bool isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        else if (n <= 3){
            return true;
        }
        else if (n % 2 == 0|| n % 3 == 0) {
            return false;
        }
        int i = 5;
        while (i * i <= n) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
            i = i + 6;
        }
        return true;
    }
};

int main() {
    Solution s;
    //cout << "result = " << s.primePalindrome(8) << endl;
    cout << "result = " << s.primePalindrome(99999) << endl;
//    cout << "result = " << s.primePalindrome(1) << endl;
//    cout << "result = " << s.primePalindrome(77) << endl;
//
//    cout << "result = " << s.primePalindrome(98) << endl;
//
//    cout << "result = " << s.primePalindrome(99) << endl;
//
//    cout << "result = " << s.primePalindrome(999999) << endl;


}