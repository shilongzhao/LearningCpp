//
// Created by Shilong Zhao on 19/07/2018.
//

/**
 *
 * 868. Binary Gap
 *
 * https://leetcode.com/problems/binary-gap/description/
 */
#include <iostream>
using namespace std;
class Solution {
public:
    int binaryGap(int N) {
        int max_gap = 0;
        while ( (N & 1) != 1 ) {
            N = N >> 1;
        }
        if (N == 1) {
            return 0;
        }
        N = N >> 1;
        max_gap = 1;

        while (N != 0) {
            int gap = 1;
            while ( (N & 1) != 1) {
                gap++;
                N = N >> 1;
            }
            if (gap > max_gap) max_gap = gap;
            N = N >> 1;
        }
        return max_gap;
    }
};

int main() {
    Solution s;
    cout << s.binaryGap(5) << endl;
    cout << s.binaryGap(6) << endl;
    cout << s.binaryGap(257) << endl;
    cout << s.binaryGap(8) << endl;



}