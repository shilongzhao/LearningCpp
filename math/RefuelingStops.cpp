//
// Created by Shilong Zhao on 23/07/2018.
//


/**
 * 871. Minimum Number of Refueling Stops
 * https://leetcode.com/problems/
 *      minimum-number-of-refueling-stops/description/
 */
#include <vector>
#include <queue>
using namespace std;

class Solution {
public:
    int minRefuelStops(int target, int cur, vector<vector<int>> &stations) {
            int i = 0, res;
            priority_queue<int> pq;
            for (res = 0; cur < target; res++) {
                while (i < stations.size() && stations[i][0] <= cur)
                    pq.push(stations[i++][1]);
                if (pq.empty()) return -1;
                cur += pq.top(), pq.pop();
            }
            return res;

    }
};