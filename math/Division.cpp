#include <climits>

/**
 * Created by Shilong Zhao on 21/08/2018.
 * 29. Divide Two Integers
 * https://leetcode.com/problems/divide-two-integers/description/
 * Bitwise devide
 * http://mathforum.org/library/drmath/view/55951.html
 */

class Solution {
public:
    int divide(int dividend, int divisor) {
        if (divisor == 0 || (dividend == INT_MIN && divisor == -1)) {
            return INT_MAX;
        }

        bool sign = (dividend > 0) ^ (divisor > 0);
        unsigned int dd = (dividend > 0) ? dividend : -dividend;
        unsigned int dv = (divisor > 0) ? divisor : -divisor;

        int r = 0;
        for (int i = 31; i >= 0; i--){
            if ( (dd >> i) >= dv){
                r = r << 1 | 0x01;
                dd -= dv << i;
            } else
                r = r << 1;
        }
        return sign? -r : r;
    }
};