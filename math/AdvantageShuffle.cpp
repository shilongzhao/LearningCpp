//
// Created by Shilong Zhao on 21/07/2018.
//

/**
 * 870. Advantage Shuffle
 * https://leetcode.com/problems/advantage-shuffle/
 */
#include <set>
#include <vector>
using namespace std;
class Solution {
public:
    vector<int> advantageCount(vector<int>& A, vector<int>& B) {
        multiset<int> s(A.begin(), A.end());
        for(int i=0;i<A.size();i++){
            auto it = *s.rbegin() > B[i] ? s.upper_bound(B[i]) : s.begin();
            A[i] = *it;
            s.erase(it);
        }
        return A;
    }
};