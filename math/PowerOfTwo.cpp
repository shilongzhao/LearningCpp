//
// Created by ZhaoShilong on 01/07/2018.
//
/* https://leetcode.com/problems/power-of-two/description/
 * 231. Power of Two
 */

#include <iostream>
using namespace std;
class Solution {
public:
    bool isPowerOfTwo(int n) {
        return (n > 0)  && (n & (n - 1)) == 0;
    }
};

int main () {
    int a = -12;
    cout << a / 10 << endl;

    cout << a % 10 << endl;
}