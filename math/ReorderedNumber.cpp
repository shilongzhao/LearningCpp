//
// Created by Shilong Zhao on 20/07/2018.
//

/**
 * 869. Reordered Power of 2
 * https://leetcode.com/problems/reordered-power-of-2/description/
 */
#include <cmath>
#include <iostream>
using namespace std;
class Solution {
public:
    bool reorderedPowerOf2(int N) {
        long stats = countDigits(N);
        for (int i = 0; i < 31; i++) {
            if (countDigits(1 << i) == stats) {
                return true;
            }
        }
        return false;
    }

    long countDigits(int N) {
        long result = 0;
        while (N != 0) {
            int d = N % 10;
            result += pow(10, d);
            N = N / 10;
        }
        return result;
    }
};

int main() {
    Solution s;
    cout << s.reorderedPowerOf2(1) << endl;  // true
    cout << s.reorderedPowerOf2(10) << endl; // true
    cout << s.reorderedPowerOf2(46) << endl; // true
    cout << s.reorderedPowerOf2(625) << endl; // true

    cout << s.reorderedPowerOf2(1104) << endl; // false

}