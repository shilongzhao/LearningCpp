//
// Created by Shilong Zhao on 05/07/2018.
//
/**
 *
 */
#include <iostream>
using namespace std;
class Solution {
public:
    int reverse(int x) {
        int r = 0;
        while (x != 0) {
            int d = x % 10;
            if (r > 0) {
                if (r > INT_MAX / 10 || d > INT_MAX - r * 10) {
                    return 0;
                }
            }
            if (r < 0) {
                if (r < INT_MIN / 10 || d < INT_MIN - r * 10) {
                    return 0;
                }
            }
            r = r * 10 + d;
            x = x / 10;
        }

        return r;
    }
};

int main() {
    Solution s;
    //cout << s.reverse(2147483647) << endl;
    //cout << s.reverse(2147483641) << endl;
    // cout << s.reverse(1534236469) << endl;
     cout << s.reverse(123) << endl;

    //cout << s.reverse(INT_MAX - 7) << endl;
    //cout << s.reverse(INT_MAX) << endl;
    //cout << s.reverse(- 2147483647 - 1) << endl;
}