//
// Created by Shilong Zhao on 02/07/2018.
//

/*
 * 207. Course Schedule

 * https://leetcode.com/problems/course-schedule/description/
 *
 */

#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;

class CourseScheduleSolution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        map<int, set<int>> graph;

        visited = vector<bool>(numCourses, false);
        onStack = vector<bool>(numCourses, false);

        for (auto &p: prerequisites) {
            if (graph.find(p.first) == graph.end()) {
                set<int> bag {p.second};
                graph.insert(make_pair(p.first, bag));
            }
            else {
                auto it = graph.find(p.first);
                it->second.insert(p.second);
            }
        }

        for (int v = 0; v < numCourses; v++) {
            if (!cycled && !visited[v]) {
                dfs(v, graph);
            }
        }
        return !cycled;
    }

private:
    vector<bool> onStack;
    vector<bool> visited;

    bool cycled = false;

    void dfs(int v, map<int, set<int>> &graph) {
        visited[v] = true;
        onStack[v] = true;
        if (graph.find(v) != graph.end()) {
            set<int> &neighbours = graph.find(v)->second;
            for (int w: neighbours) {
                if (!visited[w]) {
                    dfs(w, graph);
                }
                else if (onStack[w]){
                    cycled = true;
                    return;
                }
            }

        }
        onStack[v] = false;
    }
};

int main() {
    vector<pair<int, int>> p;
    p.push_back(make_pair(1, 0));
    p.push_back(make_pair(0, 1));
    p.push_back(make_pair(1, 2));
    p.push_back(make_pair(1, 3));
    p.push_back(make_pair(2, 3));

    CourseScheduleSolution solution;
    cout << solution.canFinish(4, p) << endl;

}