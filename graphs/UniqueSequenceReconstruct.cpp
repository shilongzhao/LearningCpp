/**
 * 444. Sequence Reconstruction
 * https://leetcode.com/problems/sequence-reconstruction/description/
 *
 * Created by Shilong Zhao on 22/08/2018.
 *
 * Transitive reduction
 */
#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    bool sequenceReconstruction(vector<int>& org, vector<vector<int>>& seqs) {
        if(seqs.empty()) return false;
        vector<int> pos(org.size() + 1);

        for(int i = 0; i < org.size(); ++i) pos[org[i]] = i;

        vector<char> flags(org.size() + 1, 0);
        int toMatch = org.size()-1;
        bool flag = false;
        for(const auto &v: seqs) {
            for(int i = 0; i < v.size(); ++i) {
                flag = true;
                if (v[i] <= 0 || v[i] > org.size()) return false;
                if (i == 0) continue;
                int x = v[i-1], y = v[i];
                if (pos[x] >= pos[y]) return false;
                if (flags[x] == 0 && pos[x] + 1 == pos[y]) flags[x] = 1, --toMatch;
            }
        }
        return toMatch == 0 && flag;
    }
};

int main() {
    Solution s;
    vector<int> org = {1,4,2,3};
    vector<vector<int>> seqs = {{1,2},{1,3},{2,3}, {4,2}, {1,4}};
    cout << s.sequenceReconstruction(org, seqs) << endl;
}