//
// Created by Shilong Zhao on 17/08/2018.
//
#include <vector>
using namespace std;

/**
 * https://leetcode.com/problems/
 *  all-paths-from-source-to-target/
 */
class Solution {
    void recursion(int n,
                   vector<vector<int>> &graph,
                   vector<int> &path,
                   vector<bool> &in_path,
                   vector<vector<int>> &all_paths) {
        if (n == graph.size() - 1) {
            vector<int> route;
            int k = 0;
            do {
                route.push_back(k);
                k = path[k];
            } while (k != n);
            route.push_back(n);
            all_paths.push_back(route);
            return;
        }

        for (int i = 0; i < graph[n].size(); i++) {
            int v = graph[n][i];
            if (!in_path[v])  {
                in_path[v] = true;
                path[n] = v;
                recursion(v, graph, path, in_path, all_paths);
                in_path[v] = false;
                path[n] = 0;
            }
        }
    }
public:
    vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
        unsigned long N = graph.size();
        vector<bool> in_path(N);
        in_path[0] = true;
        vector<int> path(N);
        vector<vector<int>> all_paths;
        recursion(0, graph, path, in_path, all_paths);
        return all_paths;
    }
};

int main() {
    vector<vector<int>> g = {
            {1,2},
            {3},
            {3},
            {}
    };
    Solution solution;
    solution.allPathsSourceTarget(g);
}
