/**
 * Created by Shilong Zhao on 21/08/2018.
 *
 * 708. Insert into a Cyclic Sorted List
 * https://leetcode.com/problems/insert-into-a-cyclic-sorted-list/description/
 */

// Definition for a Node.
class Node {
public:
    int val;
    Node* next;

    Node() {}

    Node(int _val, Node* _next) {
        val = _val;
        next = _next;
    }
};
class Solution {
public:
    Node* insert(Node* head, int v) {
        Node *t = new Node(v, nullptr);
        if (head == nullptr) {
            t->next = t;
            return t;
        }

        Node *p = head;

        while (true) {
            if (p->next->val >= p->val) {
                // handle single elements or when inside increasing sequence
                if (v >= p->val && p->next->val >= v) {
                    t->next = p->next;
                    p->next = t;
                    return head;
                }
            }
            else {
                // handle the list joint
                if (v > p->val || v < p->next->val) {
                    t->next = p->next;
                    p->next = t;
                    return head;
                }
            }
            p = p->next;
            if (p == head) {
                break;
            }
        }
        // all equal elements
        t->next = p->next;
        p->next = t;

        return head;
    }
};

int main() {
    Solution s;
    Node *h = nullptr;
    h = s.insert(h, 1);
    h = s.insert(h, 2);
    h = s.insert(h, 3);
    h = s.insert(h, 0);


}