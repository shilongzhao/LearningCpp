//
// Created by Shilong Zhao on 02/07/2018.
//

#ifndef UDEMYADVANCEDC_LISTNODE_H
#define UDEMYADVANCEDC_LISTNODE_H


struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {

    }
};
#endif //UDEMYADVANCEDC_LISTNODE_H
