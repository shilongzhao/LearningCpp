cmake_minimum_required(VERSION 3.9)
project(UdemyAdvancedC)

add_executable(Lists Lists.cpp)
add_executable(ListErase ListErase.cpp)
add_executable(RemoveNthFromEnd RemoveNthFromEnd.cpp ListNode.h)
add_executable(AddTwoNumbers AddTwoNumbers.cpp ListNode.h)
add_executable(AddingTwoNumbers AddingTwoNumbers.cpp)
add_executable(CyclicSortedListInsert CyclicSortedListInsert.cpp)