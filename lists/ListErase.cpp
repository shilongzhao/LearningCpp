//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <list>

using namespace std;

int main() {
    list<int> numbers;
    numbers.push_back(1);
    numbers.push_back(2);
    numbers.push_back(3);
    numbers.push_back(3);
    numbers.push_back(4);
    numbers.push_back(5);

    // the right way to delete elements while iterating
    list<int>::iterator it = numbers.begin();
    while (it != numbers.end()) {
        if (*it == 3) {
            it = numbers.erase(it); // erase is ineffective for vector,
            // erase() returns the new location of the element following the erased element
        }
        else {
            it++;
        }
    }

    for (int &num : numbers) {
        cout << num << endl;
    }
}