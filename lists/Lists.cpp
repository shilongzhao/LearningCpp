//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <list>

using namespace std;

int main() {
    list<int> numbers; // double linked list;

    numbers.push_back(3);
    numbers.push_back(2);
    numbers.push_back(1);  // add to tails

    numbers.push_front(4); // add to head

    list<int>::iterator it0 = numbers.begin();
    numbers.insert(it0, 99); // insert before current position of iterator,
    // insert() return the inserted element location

    // iterate with iterator
    for (auto it = numbers.begin(); it != numbers.end(); it++) { //it--, backwards
        cout << *it << " ";
    }
    cout << endl;
    // range based loop
    for (int &num : numbers) {
        cout << num << " ";
    }
    cout << endl;


}