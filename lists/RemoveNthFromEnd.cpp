//
// Created by Shilong Zhao on 02/07/2018.
//


#include "ListNode.h"
#include <iostream>

class RemoveNodeSolution {

public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (head == nullptr) {
            return nullptr;
        }
        totalNodes = 0;

        ListNode t(0); // trivial head;
        t.next = head;

        reverseDelete(&t, 0, n);

        return t.next;

    }
private:
    int totalNodes;
    void reverseDelete(ListNode *head, int currentNodeIndex, int n) {
        if (head->next == nullptr) {
            totalNodes = currentNodeIndex;
        }
        else {
             // index starts from 0, 0 is trivial head
            reverseDelete(head->next, currentNodeIndex + 1, n);
            // nodes index to delete = totalNodes - n + 1
            if (currentNodeIndex == totalNodes - n ) {
                head->next = head->next->next;
            }
        }
    }
};

int main() {

}