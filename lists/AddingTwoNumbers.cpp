//
// Created by Shilong Zhao on 04/07/2018.
//
#include "ListNode.h"

/**
 *
 * 2. Add Two Numbers

 * https://leetcode.com/problems/add-two-numbers/description/
 */
class AddingTwoNumbersSolution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int carry = 0;
        auto *head = new ListNode(0);
        auto *p = head;
        while(l1 != nullptr || l2 != nullptr || carry != 0) {

            int v1 = l1 == nullptr ? 0 : l1->val;
            int v2 = l2 == nullptr ? 0 : l2->val;

            int sum = v1 + v2 + carry;
            carry = sum / 10;
            auto *t = new ListNode(sum % 10);
            p->next = t;
            p = p->next;

            if (l2 != nullptr) l2 = l2->next;
            if (l1 != nullptr) l1 = l1->next;
        }
        p = head->next;
        delete head;

        return p;
    }
};