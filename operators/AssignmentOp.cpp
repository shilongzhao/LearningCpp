//
// Created by ZhaoShilong on 01/07/2018.
//

#include <iostream>
using namespace std;
class Student {
private:
    int id;
    string name;
public:
    Student() = default;
    Student(int id, string name): id(id), name(name) {

    }

    Student(const Student &other) :id(other.id), name(other.name){
        cout << "copy constructor running" << endl;
    }
    void print() const { // remember the const
        cout << "student[" << id << ", " << name << "]" << endl;
    }
    bool operator < (Student const &other) const { //
        return name < other.name;
    }
    const Student &operator = (const Student &other) {
        cout << "assignment running" << endl;
        id = other.id;
        name = other.name;
        return *this;
    }
    // Rule of three:
    // assigment operator, destructor, copy constructor, must be all implemented or none implemented
};

int main() {
    Student s1 = Student(1, "mike");
    Student s2 = Student(2, "bob");
    s2 = s1;
    // s2.operator=(s1);
    // without overriding = operator, the default assignment behavior is shallow copy each member value.
    s2.print();
    //NOTE that if an object does not have initialization value and assign it immediately, then the COPY constructor is
    // called, NOT the assignment constructor
    Student s3 = s1;
    s3.print();
}