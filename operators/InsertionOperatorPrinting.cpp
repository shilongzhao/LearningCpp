//
// Created by ZhaoShilong on 01/07/2018.
//

#include <iostream>
using namespace std;
class Student {
private:
    int id;
    string name;
public:
    Student() = default;
    Student(int id, string name): id(id), name(name) {

    }

    Student(const Student &other) :id(other.id), name(other.name){
        cout << "copy constructor running" << endl;
    }

    bool operator < (Student const &other) const { //
        return name < other.name;
    }
    // Rule of three:
    // assigment operator, destructor, copy constructor, must be all implemented or none implemented
    const Student &operator = (const Student &other) {
        cout << "assignment running" << endl;
        id = other.id;
        name = other.name;
        return *this;
    }

    friend ostream &operator << (ostream &out, const Student &s);
};

ostream &operator << (ostream &out, const Student &s) {
    out << "student[" << s.id << ", " << s.name << "]";
    return out;
}

int main() {
    Student m(1, "mike");
    cout << m << endl;
}
