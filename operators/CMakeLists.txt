cmake_minimum_required(VERSION 3.9)
project(UdemyAdvancedC)

set(CMAKE_CXX_STANDARD 11)

add_executable(AssignmentOp AssignmentOp.cpp)
add_executable(InsertioinOperatorPrinting InsertionOperatorPrinting.cpp)