//
// Created by Shilong Zhao on 15/08/2018.
//
#include <vector>
#include "TreeNode.h"
using namespace std;
/**
 * https://leetcode.com/problems/maximum-binary-tree/
 */
class Solution {
    TreeNode *construct(vector<int>::iterator start, vector<int>::iterator end) {
        if (start >= end) {
            return nullptr;
        }
        auto m = max_element(start, end);
        TreeNode *node = new TreeNode(*m);
        node->left = construct(start, m);
        node->right = construct(m + 1, end);
        return node;
    }
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        return construct(nums.begin(), nums.end());
    }
};