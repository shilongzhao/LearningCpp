//
// Created by Shilong Zhao on 17/08/2018.
//

#include "TreeNode.h"

/**
 *
 * https://leetcode.com/problems/binary-tree-pruning/
 */
class Solution {
    bool containsOnlyZery(TreeNode *subtree) {
        if (subtree == nullptr) return true;
        return containsOnlyZery(subtree->left)
                && containsOnlyZery(subtree->right)
                && (subtree->val == 0);
    }
public:
    TreeNode* pruneTree(TreeNode* root) {
        if (root == nullptr) {
            return nullptr;
        }
        pruneTree(root->left);
        pruneTree(root->right);

        if (containsOnlyZery(root->left)) {
            root->left = nullptr;
        }

        if (containsOnlyZery(root->right)) {
            root->right = nullptr;
        }
        return root;
    }
};