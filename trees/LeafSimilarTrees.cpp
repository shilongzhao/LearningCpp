//
// Created by Shilong Zhao on 12/08/2018.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
#include "TreeNode.h"
#include <vector>
using namespace std;

class LeafSimilarTrees {
public:
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {

        vector<int> v1;
        vector<int> v2;
        getLeaves(root1, v1);
        getLeaves(root2, v2);
        if (v1.size() != v2.size()) {
            return false;
        }

        for (int i = 0; i < v1.size(); i++) {
            if (v1[i] != v2[i]) {
                return false;
            }
        }
        return true;
    }

    void getLeaves(TreeNode *root, vector<int> &leaves) {
        if (root == nullptr) {
            return;
        }
        if (isLeafNode(root)) {
            leaves.push_back(root->val);
            return;
        }

        getLeaves(root->left, leaves);
        getLeaves(root->right, leaves);
    }


    bool isLeafNode(TreeNode *root) {
        return (root != nullptr)
                && (root->left == nullptr)
               && (root->right == nullptr);
    }
};