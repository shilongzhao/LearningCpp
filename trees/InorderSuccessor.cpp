//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 *
 * 285. Inorder Successor in BST

 * https://leetcode.com/problems/inorder-successor-in-bst/description/
 *
 */
#include "TreeNode.h"
#include <iostream>

using namespace std;

class InorderSuccessorSolution {
public:
    TreeNode* inorderSuccessor(TreeNode* root, TreeNode* p) {
        if (root == nullptr) {
            return nullptr;
        }
        if (root->val == p->val) {
            return first(root->right);
        }
        else if (p->val < root->val) {
            TreeNode *lastInLeft = last(root->left);
            if (p->val == lastInLeft->val) {
                return root;
            }
            else {
                return inorderSuccessor(root->left, p);
            }
        }
        else {
            return inorderSuccessor(root->right, p);
        }

    }
private:
    TreeNode *first(TreeNode* root) {
        if (root == nullptr) {
            return nullptr;
        }
        while (root->left != nullptr) {
            root = root->left;
        }
        return root;
    }
    TreeNode *last(TreeNode *root) {
        if (root == nullptr) {
            return nullptr;
        }
        while (root->right != nullptr) {
            root = root->right;
        }
        return root;
    }
};