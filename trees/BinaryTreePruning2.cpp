//
// Created by Shilong Zhao on 17/08/2018.
//

#include "TreeNode.h"

/**
 *
 * https://leetcode.com/problems/binary-tree-pruning/
 */
class Solution {
    bool prune(TreeNode *subtree) {
        if (subtree == nullptr) return true;
        bool l = prune(subtree->left);
        bool r = prune(subtree->right);

        if (l) subtree->left = nullptr;
        if (r) subtree->right = nullptr;

        return (subtree->val == 0) && l && r;

    }
public:
    TreeNode* pruneTree(TreeNode* root) {
        prune(root);
        return root;
    }
};