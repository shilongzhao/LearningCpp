//
// Created by ZhaoShilong on 01/07/2018.

//
/**
 * 94. Binary Tree Inorder Traversal

 * https://leetcode.com/problems/binary-tree-inorder-traversal/description/
 *
 */

#include "TreeNode.h"
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> result;
        inorder(result, root);
        return result;
    }

    void inorder(vector<int> &v, TreeNode *root) {
        if (root == nullptr) {
            return;
        }
        inorder(v, root->left);
        v.push_back(root->val);
        inorder(v, root->right);
    }
};