//
// Created by Shilong Zhao on 14/08/2018.
//

#include "TreeNode.h"

using namespace std;

/**
 * https://leetcode.com/problems/merge-two-binary-trees/description/
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
        if (t1 == nullptr && t2 == nullptr) {
            return nullptr;
        }
        int x = 0;
        if (t1 != nullptr) x += t1->val;
        if (t2 != nullptr) x += t2->val;
        auto *node = new TreeNode(x);
        node->left = mergeTrees(t1->left, t2->left);
        node->right = mergeTrees(t1->right, t2->right);

        return node;
    }
};