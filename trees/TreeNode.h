//
// Created by ZhaoShilong on 01/07/2018.
//

#ifndef UDEMYADVANCEDC_TREENODE_H
#define UDEMYADVANCEDC_TREENODE_H


#include <cstddef>

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

#endif //UDEMYADVANCEDC_TREENODE_H
