//
// Created by Shilong Zhao on 02/07/2018.
//

#include "TreeNode.h"
#include <vector>
#include <map>
using namespace std;
/*
 * 366. Find Leaves of Binary Tree
 * https://leetcode.com/problems/find-leaves-of-binary-tree/description/
 */
class Solution {
public:
    vector<vector<int>> findLeaves(TreeNode* root) {
        if (root == nullptr) {
            return vector<vector<int>>();
        }
        getNodeLevels(root);

        int rootLevel = nodeLevels.find(root)->second;
        vector<vector<int>> result(rootLevel + 1, vector<int>());
        for (auto &p : nodeLevels) {
            int nodeVal = (p.first)->val;
            int nodeLevel = p.second;
            result[nodeLevel].push_back(nodeVal);

        }
        return result;
    }


private:
    map<TreeNode *, int> nodeLevels;
    void getNodeLevels(TreeNode *root) {

        if (root == nullptr) {
            return;
        }

        if (root->left == nullptr && root->right == nullptr) {
            nodeLevels.insert(make_pair(root, 0)); // leaves
            return;
        }

        int level = 0;
        if (root->left != nullptr) {
            getNodeLevels(root->left);
            level = nodeLevels.find(root->left)->second + 1;
        }

        if (root->right != nullptr) {
            getNodeLevels(root->right);
            level = max(level, nodeLevels.find(root->right)->second + 1);
        }

        nodeLevels.insert(make_pair(root, level));
    }
};

