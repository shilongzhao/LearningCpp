//
// Created by Shilong Zhao on 16/07/2018.
//

/**
 * 863. All Nodes Distance K in Binary Tree
 * https://leetcode.com/problems/all-nodes-distance-k-in-binary-tree/
 *
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

#include "TreeNode.h"
#include <vector>
#include <iostream>
#include <unordered_map>
using namespace std;

class DistanceKSolution {
    unordered_map<TreeNode *, vector<TreeNode *>> neighbors;
    unordered_map<TreeNode *, bool> visited;
    vector<int> result;
public:
    vector<int> distanceK(TreeNode* root, TreeNode* target, int K) {
        walk(nullptr, root);
        findDistance(target, K);
        return result;
    }

    void walk(TreeNode *parent, TreeNode *node) {
        if (node == nullptr) {
            return;
        }
        vector<TreeNode *> nodes;
        if (parent != nullptr) nodes.push_back(parent);
        if (node->left != nullptr) nodes.push_back(node->left);
        if (node->right != nullptr) nodes.push_back(node->right);
        neighbors.insert(make_pair(node, nodes));

        walk(node, node->left);
        walk(node, node->right);
    }

    void findDistance(TreeNode *start, int dist) {
        if (start == nullptr || visited[start]) {
            return;
        }
        visited[start] = true;
        if (dist == 0) {
            result.push_back(start->val);
            return;
        }

        vector<TreeNode *> linked = neighbors[start];
        for (auto n: linked) {
            findDistance(n, dist - 1);
        }
    }
};