//
// Created by Shilong Zhao on 19/07/2018.
//

#include "TreeNode.h"
#include <algorithm>
#include <unordered_map>
using namespace std;
class Solution {
public:
    TreeNode* subtreeWithAllDeepest(TreeNode* root) {
        if (root == nullptr) {
            return nullptr;
        }
        auto dl = depth(root->left);
        auto dr = depth(root->right);
        if (dl.second == dr.second) {
            return root;
        }
        else if (dl.second > dr.second) {
            return subtreeWithAllDeepest(root->left);
        }
        else {
            return subtreeWithAllDeepest(root->right);
        }
    }

    pair<TreeNode *,int> depth(TreeNode *root) {
        if (root == nullptr) {
            return make_pair(root, 0);
        }
        auto left = depth(root->left);
        auto right = depth(root->right);
        int d = max(left.second, right.second) + 1;
        return make_pair(root, d);
    }
};

int main() {
    TreeNode t1 = TreeNode(1);
    TreeNode t2 = TreeNode(2);
    t1.left = &t2;
    Solution s;
    s.subtreeWithAllDeepest(&t1);
}