//
// Created by ZhaoShilong on 30/06/2018.
//
#include <iostream>
#include <set>
using namespace std;
class Student {
private:
    int id;
    string name;
public:
    Student() = default;
    Student(int id, string name): id(id), name(name) {

    }
    void print() const { // remember the const
        cout << "student[" << id << ", " << name << "]" << endl;
    }
    bool operator < (Student const &other) const { //
        return name < other.name;
    }
};

int main() {
    set<Student> students;
    students.insert(Student(1, "mike"));
    students.insert(Student(2, "vick"));
    students.insert(Student(3, "alex"));
    students.insert(Student(5, "alex")); // will not be inserted

    students.insert(Student(4, "laura"));

    for (auto &s: students) {
        s.print();
    }
}