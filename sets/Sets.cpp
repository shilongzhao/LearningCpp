//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <set>
using namespace std;

int main() {
    set<int> numbers;
    numbers.insert(50);
    numbers.insert(40);
    numbers.insert(30);
    numbers.insert(10);
    numbers.insert(20);
    numbers.insert(10);

    for (const auto &num: numbers) {
        cout << num << endl; // ordered output, no duplicate in set
    }


    auto wh = numbers.find(40);
    if (wh != numbers.end()) {
        cout << "found 40 " <<endl;
    }

    if (numbers.count(30)) {
        cout << "count 30 = " << numbers.count(30) << endl;
    }
}