//
// Created by Shilong Zhao on 07/07/2018.
//
/*
 * 860. Lemonade Change
 * https://leetcode.com/problems/lemonade-change/description/
 */
#include <vector>
#include <iostream>
using namespace std;

class LemonadeChangeSolution {
public:
    bool lemonadeChange(vector<int>& bills) {
        vector<int> notesCount(2,0);
        for (int b: bills) {
            int index = b / 5 - 1;
            int r = b - 5;
            while (r >= 10 && notesCount[1] > 0) {
                r = r - 10;
                notesCount[1] = notesCount[1] - 1;
            }

            while (r >= 5 && notesCount[0] > 0) {
                r = r - 5;
                notesCount[0] = notesCount[0] - 1;
            }
            if (r == 0) {
                if (b <= 10) {
                    notesCount[index] = notesCount[index] + 1;
                }
            }
            else {
                return false;
            }
        }
        return true;
    }
};

int main() {
    LemonadeChangeSolution solution;
    vector<int> changes({5,5,5,10,20});


    solution.lemonadeChange(changes);
}