//
// Created by ZhaoShilong on 02/07/2018.
//

#include <string>
#include <iostream>
using namespace std;

struct Test {
    virtual bool operator() (string &text) = 0;
    virtual ~Test(){}
};

struct MatchTest: public Test {
    virtual bool operator() (string &text) {
        return text == "lion";
    }
};

void check(string text, Test &testor) {
    if (testor(text)) {
        cout << "check is ok" << endl;
    }
    else {
        cout << "KO" << endl;
    }
}

int main() {
    MatchTest pred;
    string value = "lion";
    check(value, pred);
}