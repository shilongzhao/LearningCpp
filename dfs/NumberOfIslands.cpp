//
// Created by Shilong Zhao on 02/07/2018.
//

/*
 * 200. Number of Islands

 * https://leetcode.com/problems/number-of-islands/description/
 */
#include <vector>
#include <iostream>
using namespace std;

class Solution {
private:
    int numberOfIslands = 0;
public:
    void dfs(int i, int j, vector<vector<char>> &grid) {
        if (grid[i][j] == '0' || grid[i][j] == '2') {
            return;
        }
        if (grid[i][j] == '1') {
            grid[i][j] = '2';
            if (i != 0 ) {
                dfs(i - 1, j, grid);
            }
            if (i != grid.size() - 1) {
                dfs(i + 1, j, grid);
            }

            if (j != 0) {
                dfs(i, j - 1, grid);
            }
            if (j != grid[i].size() - 1) {
                dfs(i, j + 1, grid);
            }
        }
    }

    int numIslands(vector<vector<char>>& grid) {
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[i].size(); j++) {
                if (grid[i][j] == '1') {
                    numberOfIslands++;
                    dfs(i, j, grid);
                }
            }
        }
        return numberOfIslands;

    }
};