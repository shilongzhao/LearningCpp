cmake_minimum_required(VERSION 3.9)
project(UdemyAdvancedC)

set(CMAKE_CXX_STANDARD 11)

add_executable(NumberOfIslands NumberOfIslands.cpp)
add_executable(GetAllKeys GetAllKeys.cpp)
add_executable(SurroundedRegions SurroundedRegions.cpp)