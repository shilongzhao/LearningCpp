/**
 * 130. Surrounded Regions
 * https://leetcode.com/problems/surrounded-regions/description/
 *
 * Created by Shilong Zhao on 26/08/2018.
 */
#include <vector>
using namespace std;

class Solution {
    void dfs(vector<vector<char>> &board, int i, int j) {
        if (i < 0 || i >= board.size()) return;
        if (j < 0 || j >= board[0].size()) return;
        if (board[i][j] != 'O') return;

        board[i][j] = '*';
        dfs(board, i - 1, j);
        dfs(board, i + 1, j);
        dfs(board, i, j - 1);
        dfs(board, i, j + 1);
    }
public:
    void solve(vector<vector<char>>& board) {
        int r = board.size();
        if (r == 0) return;
        int c = board[0].size();
        if (c == 0) return;
        for (int i = 0; i < r; i++) {
            dfs(board, i, 0);
            dfs(board, i, c - 1);
        }

        for (int j = 1; j < c - 1; j++) {
            dfs(board, 0, j);
            dfs(board, r - 1, j);
        }

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (board[i][j] == '*') board[i][j] = 'O';
                else if (board[i][j] == 'O') board[i][j] = 'X';
            }
        }
    }
};

int main() {
    vector<vector<char>> board({
            {'X','X','X','X'},
            {'X','X','X','X'},
            {'X','X','X','X'},
            {'X','X','X','X'}
    });
}