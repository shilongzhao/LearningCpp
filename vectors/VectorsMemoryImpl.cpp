//
// Created by ZhaoShilong on 29/06/2018.
//

#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<double> nums(5);

    int capacity = nums.capacity();
    for (int i = 0; i < 10000; i++) {
        nums.push_back(i);
        if (capacity != nums.capacity()) {
            cout << "capacity " << nums.capacity();
            // everytime capacity is used up, it is doubled up, 5 - 10 - 20 - 40, etc
            // reallocate and copy
            cout << " size " << nums.size() << endl;
            capacity = nums.capacity();
        }
    }
}