//
// Created by ZhaoShilong on 29/06/2018.
//

#include <iostream>
#include <vector>

using namespace std;

void test1() {
    vector<string> str_vect(5);
    cout << "test 1" << endl;
    str_vect.push_back("one"); // starts from index 6
    str_vect.push_back("two");
    str_vect.push_back("tre");
    str_vect.push_back("four");
    cout << "size is " << str_vect.size() << endl; // size is 9
    for (int i = 0; i < str_vect.size(); i++) {
        cout << i << " " << str_vect[i] << endl;
    }
}
/**
 *
size is 9
0
1
2
3
4
5 one
6 two
7 tre
8 four
 */
void test2() {
    vector<string> str_vect;
    cout << "=== test 2 === " <<endl;
    str_vect.push_back("one"); // starts from index 0
    str_vect.push_back("two");
    str_vect.push_back("tre");
    str_vect.emplace_back("four");
    cout << "size is " << str_vect.size() << endl; // size is 9
    for (int i = 0; i < str_vect.size(); i++) {
        cout << i << " " << str_vect[i] << endl;
    }

//    vector<string>::iterator it = str_vect.begin();
//    while (it != str_vect.end()) {
//        cout << *it << endl;
//        it++;
//    }

}
/*
 *
size is 4
0 one
1 two
2 tre
3 four
 */

int main() {
    test1();
    test2();
}
