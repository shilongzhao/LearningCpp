//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 *
 * 463. Island Perimeter
 * https://leetcode.com/problems/island-perimeter/description/
 */

#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid) {
        int count = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[i].size(); j++) {
                if (grid[i][j] == 1) {
                    count += 4;
                    // only do substraction when grid[i][j] == 1;
                    if (i != 0 && grid[i - 1][j] == 1) {
                        count--;
                    }
                    if (i != grid.size() - 1 && grid[i + 1][j] == 1) {
                        count--;
                    }
                    if (j != 0 && grid[i][j - 1] == 1) {
                        count--;
                    }
                    if (j != grid[i].size() - 1 && grid[i][j + 1] == 1) {
                        count--;
                    }
                }

            }
        }
        return count;
    }
};

int main( ) {
    vector<vector<int>> g {{0,1,0,0}, {1,1,1,0}, {0,1,0,0}, {1,1,0,0}};
    Solution s;
    int r = s.islandPerimeter(g);
    cout << r << endl;
}