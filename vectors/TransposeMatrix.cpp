//
// Created by Shilong Zhao on 19/07/2018.
//
/**
 * 867. Transpose Matrix
 * https://leetcode.com/problems/transpose-matrix/description/
 */

#include <vector>
using namespace std;
class Solution {
public:
    vector<vector<int>> transpose(vector<vector<int>>& A) {
        int r = A.size();
        int c = A[0].size();

        vector<vector<int>> result(c, vector<int>(r));
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                result[j][i] = A[i][j];
            }
        }

        return result;
    }
};

int main() {

}