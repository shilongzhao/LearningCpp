//
// Created by ZhaoShilong on 29/06/2018.
//

#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool canJump(vector<int> &nums) {
        unsigned long nums_size = nums.size();
        int farthest = 0;
        int i;
        for (i = 0; i <= farthest && i < nums_size; i ++) {
            if (nums[i] + i > farthest) {
                farthest = nums[i] + i;
            }
        }

        return farthest >= nums_size - 1;
    }
};