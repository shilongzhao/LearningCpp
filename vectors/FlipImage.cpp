//
// Created by Shilong Zhao on 14/08/2018.
//

#include <vector>
using namespace std;
/**
 * https://leetcode.com/problems/flipping-an-image/description/
 */
class Solution {
public:
    vector<vector<int>> flipAndInvertImage(vector<vector<int>>& A) {
        for (auto &v : A) {
            reverse(v.begin(), v.end());
            for (auto &i: v) {
                i ^= 1;
            }
        }
        return A;
    }
};