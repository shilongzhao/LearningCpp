//
// Created by Shilong Zhao on 15/08/2018.
//
#include <vector>
using namespace std;
class Solution {
public:
    int maxIncreaseKeepingSkyline(vector<vector<int>>& grid) {
        vector<int> row_max(grid.size());
        vector<int> col_max(grid[0].size());

        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] > row_max[i]) row_max[i] = grid[i][j];
                if (grid[i][j] > col_max[j]) col_max[j] = grid[i][j];
            }
        }

        int inc = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                inc += min(row_max[i], col_max[j]) - grid[i][j];
            }
        }
        return inc;
    }
};