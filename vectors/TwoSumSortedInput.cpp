// 167. Two Sum II - Input array is sorted

// Created by Shilong Zhao on 04/07/2018.
//

/*+
 * 167. Two Sum II - Input array is sorted
    https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/
 */
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;
class TwoSumSortedInputSolution {
public:
    static vector<int> twoSum(vector<int>& numbers, int target) {
        unordered_map<int, int> numIndexMap;

        for (int i = 0; i < numbers.size(); i++) {
            auto it = numIndexMap.find(target - numbers.at(i));
            if (it != numIndexMap.end()) {
                return vector<int>({it->second + 1, i + 1});
            }
            numIndexMap.insert({numbers.at(i), i});
        }
    }
};

int main() {
    vector<int> input{1,2,5,6,7,8,9,3};

    vector<int> result = TwoSumSortedInputSolution::twoSum(input, 14);
    for (int x : result) {
        cout << x << endl;
    }

}

/*
 * Will this really work???
public int[] twoSum(int[] numbers, int target) {

    int n= numbers.length;
    int i=0;
    int j=n-1;
    int[] a=new int[2];
    while(i<j)
    {
        if(numbers[i]+numbers[j]>target) j--;
        else if(numbers[i]+numbers[j]<target) i++;
        else if(numbers[i]+numbers[j]==target)
        {
            a[0]=i+1;
            a[1]=j+1;
            return a;
        }
    }
    return a;
}
*/
