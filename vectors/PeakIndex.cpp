//
// Created by Shilong Zhao on 15/08/2018.
//

#include <vector>
using namespace std;
/**
 * https://leetcode.com/problems/peak-index-in-a-mountain-array/description/
 */
class Solution {
public:
    int peakIndexInMountainArray(vector<int>& A) {
        for (int i = 0; i < A.size(); i++) {
            if (i == A.size() - 1) {
                return i;
            }
            if (A[i + 1] < A[i]) {
                return i;
            }
        }
    }
};