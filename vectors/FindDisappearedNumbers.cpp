//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 * 448. Find All Numbers Disappeared in an Array
 * https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/description/
 * 如果我们做一个图，假设nums[a]的值为b，那么可以视作有一个edge从a指向b，如果把所有的edge都画出来
 * 那么没有incident edge的位置就是缺失的数字
 */

#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {

        for (int i = 0; i < nums.size(); i++) {
            int to = nums[i];
            while (to != 0) {
                // VISIT position to
                int t = nums[to - 1];
                nums[to - 1] = 0; // it has incident edge
                to = t;
            }
        }
        vector<int> result;
        result.reserve(nums.size());
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] != 0) {
                result.push_back(i + 1);
            }
        }
        return result;
    }
};

int main() {
    Solution solution;
    vector<int> v {4,3,2,7,8,2,3,1};
    vector<int> result = solution.findDisappearedNumbers(v);
    for (int r: result) {
        cout << "missing = " << r << endl;
    }
}