//
// Created by ZhaoShilong on 01/07/2018.
//
/**
 * https://leetcode.com/problems/plus-one/description/
 *
 * it's not OK to convert the array to a long or long long, since it may overflow
 */
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {

        int carry = 1;
        for (int i = digits.size() - 1; i >= 0; i--) {
            int r = digits[i] + carry;
            digits[i] = r % 10;
            carry = r / 10;
            if (carry == 0) {
                return digits;
            }
        }
        vector<int> result;
        result.reserve(digits.size() + 1);
        result.push_back(carry);
        for (int &d: digits) {
            result.push_back(d);
        }
        return result;
    }
};

int main() {
    Solution s;
    vector<int> digits = {9};
    vector<int> result = s.plusOne(digits);
    for (int d: result){
        cout << d << " " ;
    }
    cout << endl;

}
