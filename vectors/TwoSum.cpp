/*+
 * 1. Two Sum
 * https://leetcode.com/problems/two-sum/description/
 */

#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class TwoSumSolution {
public:
    static vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> numIndexMap;

        for (int i = 0; i < nums.size(); i++) {
            auto it = numIndexMap.find(target - nums.at(i));
            if (it != numIndexMap.end()) {
                return vector<int>({it->second, i});
            }
            numIndexMap.insert({nums.at(i), i});
        }
    }

};

int main() {
    vector<int> nums = {1,2,3,4,7,5,6};
    vector<int> result = TwoSumSolution::twoSum(nums, 9);
    for (int i : result) {
        cout << i << " ";
    }
}