//
// Created by ZhaoShilong on 30/06/2018.
//

#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<vector<int>> grid(3,vector<int>(4, 7)); // 3 rows 4 columns, initialized to 7s.
    grid[1].push_back(7); // each row does not have to be the same size
    for (int i = 0; i < grid.size(); i++) {
        for (int j = 0; j < grid[i].size();j++) {
            cout << grid[i][j] << " " << flush;
        }
        cout << endl;
    }
    return 0;
}