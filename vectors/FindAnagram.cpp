//
// Created by ZhaoShilong on 01/07/2018.
//
/**
 * 760. Find Anagram Mappings
 * https://leetcode.com/problems/find-anagram-mappings/description/
 *
 */

#include <vector>
#include <map>
using namespace std;

class Solution {
public:
    vector<int> anagramMappings(vector<int>& A, vector<int>& B) {
        map<int, int> positions;
        for (int i = 0; i < B.size(); i++) {
            positions[B[i]] = i;
        }

        vector<int> result;
        result.reserve(A.size());
        for (const int &a : A) {
            result.push_back( positions[a] );
        }

        return result;
    }
};