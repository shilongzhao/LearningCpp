//
// Created by ZhaoShilong on 01/07/2018.

//

/*
 * 387. First Unique Character in a String

 * https://leetcode.com/problems/first-unique-character-in-a-string/description/
 *
 */

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int firstUniqChar(string s) {
        vector<int> positions(26, -1);
        for (int i = 0; i < s.size();i++) {
            char c = s[i];
            if (positions[c - 'a'] == -1 ) {
                positions[c - 'a'] = i;
            }
            else if (positions[c - 'a'] >= 0){
                positions[c - 'a'] = -2;
            }
        }
        int min = INT_MAX;
        for (int i = 0; i < positions.size(); i++) {
            if (positions[i] >= 0 && positions[i] < min) {
                min = positions[i];
            }
        }
        if (min == INT_MAX) {
            return -1;
        }
        return min;
    }
};

int main() {
    string s1 = "leetcode";
    string s2 = "loveleetcode";
    Solution s;
    cout << s.firstUniqChar(s1) << endl;
    cout << s.firstUniqChar(s2) << endl;

}