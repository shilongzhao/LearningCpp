//
// Created by Shilong Zhao on 05/07/2018.
//

/**
 * 56. Merge Intervals
 * https://leetcode.com/problems/merge-intervals/description/
 */
#include <iostream>
#include <vector>

using namespace std;

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

class MergeIntervalsSolution {
public:
    vector<Interval> merge(vector<Interval>& intervals) {
        vector<Interval> result;
        if (intervals.size() == 0) return result;
        sort(intervals.begin(), intervals.end(), [](Interval &a, Interval &b) -> bool {
            return a.start < b.start;
        });

        auto current = intervals[0];
        for (int i = 1; i < intervals.size(); i++) {
            if (intervals[i].start <= current.end) {
                current.end = max(intervals[i].end, current.end);
            }
            else {
                result.push_back(current);
                current = intervals[i];
            }
        }
        result.push_back(current);
        return result;
    }
};

int main() {

    vector<Interval> intervals;
    intervals.push_back({2,8});
    intervals.push_back({8,10});
    intervals.push_back({10,18});
    intervals.push_back({1,3});

    Solution s;
    s.merge(intervals);

}
