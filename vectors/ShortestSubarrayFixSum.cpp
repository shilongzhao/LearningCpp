//
// Created by Shilong Zhao on 11/07/2018.
//
/**
 *
 * 862. Shortest Subarray with Sum at Least K
 *
 * https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/
 *
 * https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/
 * discuss/143726/C++JavaPython-O(N)-Using-Deque
 */
#include <vector>
#include <deque>
#include <iostream>

using namespace std;
class ShortestSubarraySolution {
public:
    int shortestSubarray(vector<int> A, int K) {
        int N = A.size(), res = N + 1;
        vector<int> B(N + 1, 0);
        for (int i = 0; i < N; i++) B[i + 1] += B[i] + A[i];
        deque<int> d;
        for (int i = 0; i < N + 1; i++) {
            while (d.size() > 0 && B[i] - B[d.front()] >= K)
                res = min(res, i - d.front()), d.pop_front();
            while (d.size() > 0 && B[i] <= B[d.back()]) d.pop_back();
            d.push_back(i);
        }
        return res <= N ? res : -1;
    }

};

int main() {
    ShortestSubarraySolution solution;
    cout << solution.shortestSubarray({45,95,97,-34,-42}, 21) << endl; // 1

    // cout << solution.shortestSubarray({-28,81,-20,28,-29}, 89) << endl; // 3

}