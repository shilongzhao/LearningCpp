//
// Created by ZhaoShilong on 29/06/2018.
//
/**
 * 45. Jump Game II
 * https://leetcode.com/problems/jump-game-ii/description/
 */
#include <vector>
using namespace std;

class Solution {
public:
    int jump(vector<int> &nums) {
        int furthest = 0;
        int min_steps = 0;
        int last_furthest = 0;

        for (int i = 0; i < nums.size(); i++) {
            if (i <= last_furthest) {
                furthest = max(furthest, i + nums[i]);
            }
            else if (i > last_furthest) {
                last_furthest = furthest;
                furthest = max(furthest, i + nums[i]);
                min_steps += 1;
            }
        }

        return min_steps;
    }
};