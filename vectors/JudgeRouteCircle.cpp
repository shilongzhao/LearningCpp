//
// Created by ZhaoShilong on 01/07/2018.
//

/*
 * https://leetcode.com/problems/judge-route-circle/description/
 * 657. Judge Route Circle
 *
 */

class Solution {
public:
    bool judgeCircle(string moves) {
        int x = 0, y = 0;
        for (char &c: moves) {
            switch (c) {
                case 'L':
                    x -= 1;
                    break;
                case 'R':
                    x += 1;
                    break;
                case 'U':
                    y += 1;
                    break;
                case 'D':
                    y -= 1;
                    break;
                default:
                    break;
            }
        }
        return x == 0 && y == 0;
    }
};