//
// Created by Shilong Zhao on 15/08/2018.
//

#include <string>
#include <vector>
#include <iostream>
using namespace std;

/**
 * https://leetcode.com/problems/output-contest-matches/
 */
class Solution {
    string makeMatches(vector<string> &matches) {
        if (matches.size() == 1) {
            return matches[0];
        }
        vector<string> nextRound (matches.size()/2);
        for (int i = 0, j = matches.size() - 1; i < j; i++, j--) {
            nextRound[i] = "(" + matches[i] + "," + matches[j] + ")";
        }

        return makeMatches(nextRound);
    }
public:
    string findContestMatch(int n) {
        vector<string> matches(n);
        for (int i = 1; i <= n; i++) {
            matches[i - 1] = to_string(i);
        }
        return makeMatches(matches);
    }
};

int main() {
    Solution s;
    string str = s.findContestMatch(8);
    cout << str << endl;
}