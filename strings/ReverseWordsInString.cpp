/**
 * 151. Reverse Words in a String
 * Created by Shilong Zhao on 18/08/2018.
 * https://leetcode.com/problems/reverse-words-in-a-string/description/
 */
#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    void reverseWords(string &s) {
        reverse(s.begin(), s.end()); // reverse the whole string
        string r;
        int i = 0, j = 0;
        int l = static_cast<int>(s.size());
        while (s[l - 1] == ' ') l--; // trim trailing spaces

        while (i < l && j < l) {

            while (i < l && s[i] == ' ') i++; // move to the first non-space position

            if (i >= l) break;

            j = i;

            while (j < l && s[j] != ' ') j++; // find the word

            for (int k = j - 1; k >= i; k--) r.push_back(s[k]);

            if (j != l) r.push_back(' ');

            i = j + 1;
        }
        s = r;
    }
};

int main() {
    Solution s;
    string str = "  the sky is blue  ";
    s.reverseWords(str);
    cout << str << endl;

}



