//
// Created by Shilong Zhao on 04/07/2018.
//

/**
 * 3. Longest Substring Without Repeating Characters
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
 */
#include <unordered_map>
#include <string>

using namespace std;
class NoRepeatingSubstringSolution {
public:
    int lengthOfLongestSubstring(string s) {
        if (s.size() == 0) {
            return 0;
        }
        int i = 0, j = 1;
        unordered_map<char, int> positions;
        positions.insert({s[0],0 });
        int max_length = 1;
        while (j < s.size() ) {
            while (j < s.size() && (positions.find(s[j]) == positions.end() || positions.find(s[j])->second < i) ) {
                positions[s[j]] = j;
                j++;
            }

            max_length = max(max_length, j - i);

            if (j >= s.size())  break;
            i = positions[s[j]] + 1;
        }

        return max_length;
    }
};

int main() {


}