/**
 * Created by Shilong Zhao on 21/08/2018.
 *
 * 288. Unique Word Abbreviation
 * https://leetcode.com/problems/unique-word-abbreviation/description/
 */
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <iostream>

using namespace std;

class ValidWordAbbr {
    unordered_map<string, unordered_set<string>> mp;
public:
    ValidWordAbbr(vector<string> dictionary) {
        for (string& d : dictionary) {
            int n = d.length();
            string abbr = d[0] + to_string(n) + d[n - 1];
            mp[abbr].insert(d);
        }
    }

    bool isUnique(string word) {
        int n = word.length();
        string abbr = word[0] + to_string(n) + word[n - 1];
        return mp[abbr].count(word) == mp[abbr].size();
    }
};


/**
 * Your ValidWordAbbr object will be instantiated and called as such:
 * ValidWordAbbr obj = new ValidWordAbbr(dictionary);
 * bool param_1 = obj.isUnique(word);
 */