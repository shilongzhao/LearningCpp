//
// Created by Shilong Zhao on 13/08/2018.
//

// Forward declaration of the read4 API.
#include <algorithm>
using namespace std;

/**
 *
 * @param buf destination buffer, source is unknown.
 * @return  the number of chars read,
 *          return 4 if source has more than 4 chars remaining
 *          otherwise return the number of chars remaining (<= 4)
 */
int read4(char *buf);

class Solution {
public:
    /**
     * @param   buf   Destination buffer
     * @param   n     Maximum number of characters to read
     * @return      The number of characters read,
     *              you could read more than n chars to buf,
     *              but should return the min(n, res)
     *              so that the caller of this function will
     *              limit to the right range.
     */
    int read(char *buf, int n) {
        int res = 0;
        for (int i = 0; i <= n/4; i++) {
            int curr = read4(buf + res);
            res += curr;
            if (curr == 0) {
                break;
            }
        }
        return min(n, res);
    }
};