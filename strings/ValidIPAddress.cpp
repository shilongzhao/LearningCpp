/**
 * 468. Validate IP Address
 *
 * https://leetcode.com/problems/validate-ip-address/description/
 *
 * Created by Shilong Zhao on 24/08/2018.
 */
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Solution {
private:
    vector<string> splitString(string& s, char delimiter) {
        string temp = s;
        vector<string> tokens;
        do {
            int pos = temp.find(delimiter);
            tokens.push_back(temp.substr(0, pos));
            temp = temp.substr(pos + 1);
        } while (temp.find(delimiter) != string::npos);
        tokens.push_back(temp);
        return tokens;
    }

    string validateIPv4(string ip) {
        vector<string> segments = splitString(ip, '.');
        if (segments.size() != 4) {
            return "Neither";
        }

        for (string segment : segments) {
            if (!validIPv4Segment(segment)) {
                return "Neither";
            }
        }
        return "IPv4";
    }

    bool validIPv4Segment(string segment) {
        if (segment == "" || (segment.size() > 1 && segment[0] == '0')) {
            return false;
        }

        int value = 0;
        for (int i = 0; i < segment.size(); i++) {
            if (!isdigit(segment[i])) {
                return false;
            }
            value = value * 10 + (segment[i] - '0');
            if (value > 255) {
                return false;
            }
        }
        return value <= 255;
    }

    string validateIPv6(string ip) {
        vector<string> segments = splitString(ip, ':');
        if (segments.size() != 8) {
            return "Neither";
        }

        for (string segment : segments) {
            if (!validIPv6Segment(segment)) {
                return "Neither";
            }
        }
        return "IPv6";
    }

    bool validIPv6Segment(string segment) {
        int length = segment.size();
        if (length == 0 || length > 4) {
            return false;
        }

        for (char c : segment) {
            if (isdigit(c)) {
                continue;
            }
            if (c >= 'a' && c <= 'f') {
                continue;
            }
            if (c >= 'A' && c <= 'F') {
                continue;
            }
            return false;
        }
        return true;
    }
public:
    string validIPAddress(string IP) {
        if (IP.find('.') != string::npos) {
            return validateIPv4(IP);
        }

        if (IP.find(':') != string::npos) {
            return validateIPv6(IP);
        }
        return "Neither";
    }
};

int main() {
    Solution s;
    s.validIPAddress("192.168.1.1");
}