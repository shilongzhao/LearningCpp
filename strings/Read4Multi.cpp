//
// Created by Shilong Zhao on 13/08/2018.
//


/**
 * https://leetcode.com/problems/read-n-characters-given-read4-ii-call-multiple-times/description/
 */

#include <algorithm>
#include <string>
using namespace std;
// Forward declaration of the read4 API.

/**
 * read from a file and write 4 characters to buffers starting with buf
 * @param buf pointer to buffer start
 * @return  bytes read
 */
int read4(char *buf);

class Solution {
    char buffer[4];
    int  buf_size = 0; // size of buffer
    int  buf_pos = 0;
public:
    /**
     * Expected behavior: suppose file contains "abc"
     * read(buf, 1) --> buf: "a"
     * read(buf, 1) --> buf: "b"
     * read(buf, 3) --> buf: "c"
     *
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    int read(char *buf, int n) {

        int i = 0;
        while (i < n) {
            if (buf_pos < buf_size) {
                // if there's remaining unread chars inside buffer
                buf[i++] = buffer[buf_pos++];
            }
            else {
                buf_size = read4(buffer);
                if (buf_size == 0) {
                    break;
                }
                buf_pos = 0;
            }
        }
        return i;
    }
};
