//
// Created by Shilong Zhao on 13/08/2018.
//

#include <string>
using namespace std;
class Solution {
public:
    string toLowerCase(string str) {
        for (int i = 0; i < str.size(); i++) {
            if (str[i] <= 'Z' && str[i] >= 'A') {
                str[i] = str[i] - 'A' + 'a';
            }
        }

        return str;
    }
};