//
// Created by Shilong Zhao on 13/08/2018.
//

#include <string>
using namespace std;
/**
 * https://leetcode.com/problems/one-edit-distance/description/
 */
class Solution {
    bool isOneDel(string s, string t) {
        if (s.size() - t.size() != 1 ) {
            return false;
        }

        int i = 0, j = 0;
        while (s[i] == t[j]) {
            i++, j++;
        }
        return s.substr(i + 1) == t.substr(j);
    }
    bool isOneRep(string s, string t) {
        if (s.size() != t.size() || s == t) {
            return false;
        }
        int i = 0, j = 0;
        while (s[i] == t[j]) {
            i++, j++;
        }

        return  (s.substr(i + 1) == t.substr(j + 1));
    }
public:
    bool isOneEditDistance(string s, string t) {
        return isOneRep(s, t) || isOneDel(s, t) || isOneDel(t, s);
    }
};

int main() {
    Solution s;
    s.isOneEditDistance("a", "");
}