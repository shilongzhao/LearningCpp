//
// Created by Shilong Zhao on 12/08/2018.
//

#include <vector>
#include <stack>
using namespace std;
/**
 * https://leetcode.com/problems/reverse-words-in-a-string-ii/description/
 */
class ReverseWords {
public:
    void reverseWords(vector<char>& str) {
        stack<char> chars;
        vector<char> out;
        reverse(str.begin(), str.end());
        for (int i = 0; i < str.size(); i++) {
            if (str[i] != ' ') {
                chars.push(str[i]);
            }
            if ((str[i] == ' ') || (i == str.size() - 1)) {
                while (!chars.empty()) {
                    out.push_back(chars.top());
                    chars.pop();
                }
                if (i != str.size() - 1) {
                    out.push_back(' ');
                }
            }
        }
        str = out;
    }

    void reverseSeg(vector<char>& str, int st, int end)
    {
        int lo = st;
        int hi = end;

        while(lo<hi)
        {
            swap(str[lo],str[hi]);
            lo++;
            hi--;
        }

    }
    void reverseWords2(vector<char>& str) {
        int sz = str.size();

        reverseSeg(str,0,sz-1);

        int st = 0;

        while(st<sz)
        {
            while(str[st]==' ')
            {
                st++;
            }
            int en = st;
            while(en<sz and str[en]!=' ')
            {
                en++;
            }
            en--;
            reverseSeg(str,st,en);
            st = en+1;
        }
    }
};