//
// Created by Shilong Zhao on 13/08/2018.
//
/**
 * https://leetcode.com/problems/
 * longest-substring-with-at-most
 * -two-distinct-characters/description/
 */
#include <string>
#include <iostream>
using namespace std;
class Solution {
public:
    int lengthOfLongestSubstringTwoDistinct(string s) {
        if (s.size() <= 2) {
            return static_cast<int>(s.size());
        }
        int i = 0, j = 1;
        while (j < s.size() && s[j] == s[i]) {
            j++;
        }
        if (j == s.size()) {
            return s.size();
        }

        char c1 = s[i], c2 = s[j];
        int st = 0;
        int max_len = 0;
        while (j < s.size()) {
            while (s[j] == c1 || s[j] == c2) {
                if (s[j] != s[j-1]) {
                    // in case having a third character,
                    // the start position of the new sequence
                    st = j;
                }
                j++;
            }

            max_len = max(max_len, j - i);
            i = st;
            c1 = s[i], c2 = s[j];
        }

        return max(max_len, j - i);

    }
};

int main() {
    Solution s;
    cout << s.lengthOfLongestSubstringTwoDistinct("aaaxbxbbxbcccc") << endl;
    cout << s.lengthOfLongestSubstringTwoDistinct("aabbbxccc") << endl;

}