//
// Created by Shilong Zhao on 13/08/2018.
//

#include <string>
#include <vector>
#include <set>
using namespace std;

/**
 * https://leetcode.com/problems/unique-morse-code-words/description/
 */
class Solution {
    string morseEncode(string input) {
        vector<string> morse = {".-","-...","-.-.","-..",".",
                                "..-.","--.","....","..",".---",
                                "-.-",".-..","--","-.","---",".--.",
                                "--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        string res;
        for (char c: input) {
            res.append(morse[c - 'a']);
        }
        return res;
    }
public:
    int uniqueMorseRepresentations(vector<string>& words) {

        set<string> interpretation;
        for (string w: words) {
            interpretation.insert(morseEncode(w));
        }

        return interpretation.size();
    }
};

