//
// Created by Shilong Zhao on 05/07/2018.
//

/**
 * 5. Longest Palindromic Substring
 * https://leetcode.com/problems/longest-palindromic-substring/description/
 */

#include <iostream>
#include <vector>
#include <string>
using namespace std;

class LongestPalindromeSolution {
public:
    string longestPalindrome(string s) {
        vector<vector<bool>> mat(s.size(), vector<bool>(s.size(), false));
        for (int i = 0; i < s.size(); i++) {
            mat[i][i] = true;
        }

        int start = 0, num_chars = 1;
        for (int d = 1; d < s.size(); d++) {
            for (int i = 0; i + d < s.size(); i++) {
                if (d != 1)
                    mat[i][i + d] = mat[i + 1][i + d - 1] && (s[i] == s[i + d]);
                else
                    mat[i][i + d] = (s[i] == s[i + 1]);

                if (mat[i][i + d] && (d + 1 > num_chars)) {
                    start = i;
                    num_chars = d + 1;
                }
            }
        }
        return s.substr(start, num_chars);
    }
};

int main( ) {
    LongestPalindromeSolution solution;
    cout << solution.longestPalindrome("caba") << endl;
    cout << solution.longestPalindrome("cbbc") << endl;
    cout << solution.longestPalindrome("cbbd") << endl;
    cout << solution.longestPalindrome("bbbbbbbb") << endl;

}