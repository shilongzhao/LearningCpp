//
// Created by Shilong Zhao on 02/07/2018.
//

/**
 * 344. Reverse String

 * https://leetcode.com/problems/reverse-string/description/
 *
 */
#include <string>
#include <iostream>
using namespace std;

class ReverseStringSolution {
public:
    string reverseString(string s) {
        if (s.size() == 0) {
            return s;
        }
        for(unsigned long i = 0, j = s.size() - 1; i < j; i++, j--) {
            char l = s[i];
            s[i] = s[j];
            s[j] = l;
        }

        return s;
    }
};

int main() {
    string s("");
    ReverseStringSolution solution;
    cout << solution.reverseString(s) << endl;
}