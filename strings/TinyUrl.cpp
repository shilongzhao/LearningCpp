//
// Created by Shilong Zhao on 15/08/2018.
//

/**
 *
 */
#include <string>
#include <map>
using namespace std;

class Solution {
public:
    map<int, string> urls;
    int i = 0;
    // Encodes a URL to a shortened URL.
    string encode(string longUrl) {
        urls.insert(make_pair(++i, longUrl));
        return "http://tinyurl.com/" + to_string(i);
    }

    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl) {
        string id = shortUrl.replace(0, strlen("http://tinyurl.com/"), "");
        int x = stoi(id);
        if (urls.find(x)!= urls.end()) {
            return urls.find(x)->second;
        }
        return nullptr;
    }
};

// Your Solution object will be instantiated and called as such:
// Solution solution;
// solution.decode(solution.encode(url));

int main() {
    Solution s;
    string tinyUrl = s.encode("http://www.dreamhack.it");
    s.decode(tinyUrl);
}