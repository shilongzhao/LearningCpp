//
// Created by ZhaoShilong on 27/06/2018.
//

#include <iostream>
using namespace std;

int mightWrong () {
    bool error = false;


    if (error) {
        throw 8; // can throw any thing
    }

    bool error2 = false; // error2 = false
    if (error2) {
        throw "server internal error";
    }

    bool error3 = true; // false
    if (error3) {
        throw string("hello");
    }

}
// Like Java, the exception will swim up if not caught at current level
int test() {
    mightWrong();
}

int main() {
    try {
        test();
    }
    catch (int e) {
        cout << "cought exception " << e << endl;
    }
    catch (const char *s) { // cannot catch (string  s) or catch (string &s)
        cout << "caught string exception " << s << endl;
    }
    catch (string &s) { // catch should match the thrown type, use reference
        cout << "caught string exception: " << s << endl;
    }
    cout << "continue running" << endl;

    return 0;
}