//
// Created by ZhaoShilong on 28/06/2018.
//
#include <iostream>
using namespace std;

class CanGoWrong {
public:
    CanGoWrong() {
        // memory allocation can go wrong (throw exception) if required space is tooo large
        // whether or not it throws an exception depends on the compiler configuration
        char *p = new char[999999999999999];
        delete [] p;
    }
};

int main() {
    try {
        CanGoWrong wrong;
        cout << "will not print" << endl;
    } catch (bad_alloc &e) {// when an object is thrown, we should catch it by its reference
        cout << "caught error: "<<  e.what() << endl;
    }
    cout << "continue running" << endl;
}

// all c++ exceptions extends base class exception

