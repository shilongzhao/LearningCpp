//
// Created by ZhaoShilong on 28/06/2018.
//

#include <iostream>
#include <exception>

using namespace std;

void goesWrong() {
    throw bad_alloc();
}
// Note the sequence of catching, Java rules apply here
int main() {
    try {
        goesWrong();
    }
    catch (exception &e) {
        cout << "Caught general exception" << endl;
    }
    catch (bad_alloc &e) { // C++ cannot detect that this is clause is actually being shadowed at compile time
        cout << "Caught bad alloc excpetion" << endl;
    }
}