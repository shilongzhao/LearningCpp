//
// Created by ZhaoShilong on 28/06/2018.
//

#include <iostream>
#include <exception>

using namespace std;

class MyException: public exception {
public:
    virtual const char *what() const throw() { // UNLIKE Java, there is NO compile time check for what exceptions will be thrown!
        // const throw() at the end of signature means this method will not throw expction -> compiler can optimize code
        // if you want to throw exceptions, it should be: const throw(bad_alloc, my_exception) ...
        return "Message from MyException";
    }

};

class GoWrong {
public:
    void goesWrong() {
        throw MyException(); // no new operator?
    }
};
int main() {
    GoWrong test;

    try {
        test.goesWrong();
    }
    catch (MyException &e) {
        cout << e.what() << endl;
    }
}