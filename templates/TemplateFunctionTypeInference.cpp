//
// Created by ZhaoShilong on 01/07/2018.
//

#include <iostream>
using namespace std;

template<typename T>
void print(T t) {
    cout << t << endl;
}

void print(int v) {
    cout << "Non template function " << v << endl;
}

int main() {
    print<string>("hello");
    print<int>(12345);
    print(12345);
    print("hello there!");
}