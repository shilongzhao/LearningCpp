//
// Created by ZhaoShilong on 01/07/2018.
//

#include <iostream>
using namespace std;

template<typename T>
class TestTemplate {
private:
    T obj;
public:
    TestTemplate() = default;

    explicit TestTemplate(T obj) {
        this->obj = obj;
    }
    void print() {
        cout << obj << endl;
    }

};

int main() {
    TestTemplate<string> t1("hello");
    t1.print();

    TestTemplate<int> t2(2345);
    t2.print();
}